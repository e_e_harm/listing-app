# clone the project and install dependencies

- `git clone https://e_e_harm@bitbucket.org/e_e_harm/listing-app.git`

- `cd listing-app`

- `npm install`

# set up a database

- `cp config/config.example.json config/config.json`

- update to suit your database configuration, postgres is preferred

- `npx sequelize-cli db:migrate` to run migrations

- `npx sequelize-cli db:seed:all` to run seeds

# start server

- `npm start`

# basic crud

- *index* `curl http://localhost:8000/api/listings`

- *show* `curl http://localhost:8000/api/listings/1`

- *create* `curl -X POST http://localhost:8000/api/listings/ -d '{"listing": { "streetAddress": "881 7th Ave", "city": "New York", "state": "NY", "zip": "10019"}}' -H 'Content-Type: application/json'`

- *update* `curl -X PUT http://localhost:8000/api/listings/1 -d '{ "streetAddress": "321 Main St."}' -H 'Content-Type: application/json'`

- *delete* `curl -X DELETE http://localhost:8000/api/listings/2`
