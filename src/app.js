const express = require('express')
const app = express()
const port = 8000
const path = require('path')
const bodyParser = require('body-parser')

app.use(express.static(path.join(__dirname, '../public')))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'))
})

app.use('/api/listings', require('./controllers/ListingsController.js'))

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
