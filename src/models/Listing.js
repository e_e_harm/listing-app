const { DataTypes } = require('sequelize')
const db = require('./')

const Listing = db.define('Listing', {
  streetAddress: { type: DataTypes.STRING, allowNull: false },
  city: { type: DataTypes.STRING, allowNull: false },
  zip: { type: DataTypes.STRING, allowNull: false },
  state: { type: DataTypes.STRING, allowNull: false }
}, {
  tableName: 'listings'
})

module.exports = Listing
