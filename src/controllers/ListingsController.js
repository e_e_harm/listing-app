const express = require('express')
const router = express.Router()
const Listing = require('../models/Listing')

const NotFoundError = {
  message: 'Not Found'
}

router.get('/', async (req, res) => {
  try {
    const listings = await Listing.findAll()
    res.status(200).send(JSON.stringify(listings))
  } catch (err) {
    res.status(500).send(err.toString())
  }
})

router.get('/:id', async (req, res) => {
  try {
    const listing = await Listing.findOne({ where: { id: req.params.id } })
    if (listing) res.status(200).send(JSON.stringify(listing))
    else res.status(400).send(NotFoundError)
  } catch (err) {
    res.status(500).send(err.toString())
  }
})

router.post('/', async (req, res) => {
  try {
    const listing = await Listing.create(req.body.listing)
    res.status(201).send(JSON.stringify(listing))
  } catch (err) {
    res.status(500).send(err.toString())
  }
})

router.put('/:id', async (req, res) => {
  try {
    const [_, listings] = await Listing.update(
      { ...req.body },
      { returning: true, where: { id: req.params.id } }
    )
    res.status(201).send(JSON.stringify(listings[0]))
  } catch (err) {
    res.status(500).send(err.toString())
  }
})

router.delete('/:id', async (req, res) => {
  try {
    await Listing.destroy({ where: { id: req.params.id } })
    res.status(204).send(null)
  } catch (err) {
    res.status(500).send(err.toString())
  }
})

module.exports = router
